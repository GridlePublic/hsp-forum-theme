{isSpider}
		</div><!-- END container -->
	</main>
	<!-- IF !isSpider -->
	<div class="topic-search hidden">
		<div class="btn-group">
			<button type="button" class="btn btn-default count"></button>
			<button type="button" class="btn btn-default prev"><i class="fa fa-fw fa-angle-up"></i></button>
			<button type="button" class="btn btn-default next"><i class="fa fa-fw fa-angle-down"></i></button>
		</div>
	</div>

	<div component="toaster/tray" class="alert-window">
		<div id="reconnect-alert" class="alert alert-dismissable alert-warning clearfix hide" component="toaster/toast">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p>[[global:reconnecting-message, {config.siteTitle}]]</p>
		</div>
	</div>
	<!-- ENDIF !isSpider -->
	<footer class="main-footer hidden-xs">
		<div class="main-footer-content">
			<ul class="footer-nav">
				<li><a target="_blank" href="https://homeschoolpanda.com/community-rules">Community Rules</a></li>
				<li><a target="_blank" href="https://homeschoolpanda.com/privacy-policy">Privacy policy</a></li>
				<li><a  target="_blank" href="https://homeschoolpanda.com/terms-of-use">Terms of Use </a></li>
				<li><a target="_blank" href="https://medium.com/the-homeschool-panda-blog" target="_blank">Blog </a></li>
				<li class="footer-telephone">
				<svg id="icon-call" fill="#fff" height="20" width="20" viewBox="0 0 512 512" ><title>call</title><path d="M426.666,330.667c-26.666,0-52.271-4.271-75.729-11.729c-7.469-2.136-16-1.073-21.332,5.333l-46.939,46.928 c-60.802-30.928-109.864-80-140.802-140.803l46.939-46.927c5.332-5.333,7.462-13.864,5.332-21.333 c-8.537-24.531-12.802-50.136-12.802-76.803C181.333,73.604,171.734,64,160,64H85.333C73.599,64,64,73.604,64,85.333 C64,285.864,226.136,448,426.666,448c11.73,0,21.334-9.604,21.334-21.333V352C448,340.271,438.396,330.667,426.666,330.667z"></path></svg>
				<span style="margin-left:5px">(941) 312-1376</span></li>
				<li><a class="footer-email" href="mailto:contact@homeschoolpanda.com">
						<svg id="icon-email"  fill="#fff" height="20" width="20" viewBox="0 0 24 24" ><title>emaiil</title><path d="M12 11.016l8.016-5.016h-16.031zM20.016 18v-9.984l-8.016 4.969-8.016-4.969v9.984h16.031zM20.016 3.984c1.078 0 1.969 0.938 1.969 2.016v12c0 1.078-0.891 2.016-1.969 2.016h-16.031c-1.078 0-1.969-0.938-1.969-2.016v-12c0-1.078 0.891-2.016 1.969-2.016h16.031z"></path></svg>
						<span style="margin-left:5px">contact@homeschoolpanda.com</span></a>
				</li>
			</ul>
			<div class="footer-info" >
				<a href="/"><img class="footer-big-logo" src="/../../images/pandalogo-whitetext.svg"></a>
				
			</div>
			<div class="footer-social">
			<ul class="social-nav">
					<li><a href="https://www.instagram.com/homeschoolpanda/" target="_blank" rel="noopener">
					<svg id="icon-instagram"  fill="#fff" viewBox="0 0 32 32" width="30" height="32"><title>instagram</title><path d="M16 2.881c4.275 0 4.781 0.019 6.462 0.094 1.563 0.069 2.406 0.331 2.969 0.55 0.744 0.288 1.281 0.638 1.837 1.194 0.563 0.563 0.906 1.094 1.2 1.838 0.219 0.563 0.481 1.412 0.55 2.969 0.075 1.688 0.094 2.194 0.094 6.463s-0.019 4.781-0.094 6.463c-0.069 1.563-0.331 2.406-0.55 2.969-0.288 0.744-0.637 1.281-1.194 1.837-0.563 0.563-1.094 0.906-1.837 1.2-0.563 0.219-1.413 0.481-2.969 0.55-1.688 0.075-2.194 0.094-6.463 0.094s-4.781-0.019-6.463-0.094c-1.563-0.069-2.406-0.331-2.969-0.55-0.744-0.288-1.281-0.637-1.838-1.194-0.563-0.563-0.906-1.094-1.2-1.837-0.219-0.563-0.481-1.413-0.55-2.969-0.075-1.688-0.094-2.194-0.094-6.463s0.019-4.781 0.094-6.463c0.069-1.563 0.331-2.406 0.55-2.969 0.288-0.744 0.638-1.281 1.194-1.838 0.563-0.563 1.094-0.906 1.838-1.2 0.563-0.219 1.412-0.481 2.969-0.55 1.681-0.075 2.188-0.094 6.463-0.094zM16 0c-4.344 0-4.887 0.019-6.594 0.094-1.7 0.075-2.869 0.35-3.881 0.744-1.056 0.412-1.95 0.956-2.837 1.85-0.894 0.888-1.438 1.781-1.85 2.831-0.394 1.019-0.669 2.181-0.744 3.881-0.075 1.713-0.094 2.256-0.094 6.6s0.019 4.887 0.094 6.594c0.075 1.7 0.35 2.869 0.744 3.881 0.413 1.056 0.956 1.95 1.85 2.837 0.887 0.887 1.781 1.438 2.831 1.844 1.019 0.394 2.181 0.669 3.881 0.744 1.706 0.075 2.25 0.094 6.594 0.094s4.888-0.019 6.594-0.094c1.7-0.075 2.869-0.35 3.881-0.744 1.050-0.406 1.944-0.956 2.831-1.844s1.438-1.781 1.844-2.831c0.394-1.019 0.669-2.181 0.744-3.881 0.075-1.706 0.094-2.25 0.094-6.594s-0.019-4.887-0.094-6.594c-0.075-1.7-0.35-2.869-0.744-3.881-0.394-1.063-0.938-1.956-1.831-2.844-0.887-0.887-1.781-1.438-2.831-1.844-1.019-0.394-2.181-0.669-3.881-0.744-1.712-0.081-2.256-0.1-6.6-0.1v0z"></path><path d="M16 7.781c-4.537 0-8.219 3.681-8.219 8.219s3.681 8.219 8.219 8.219 8.219-3.681 8.219-8.219c0-4.537-3.681-8.219-8.219-8.219zM16 21.331c-2.944 0-5.331-2.387-5.331-5.331s2.387-5.331 5.331-5.331c2.944 0 5.331 2.387 5.331 5.331s-2.387 5.331-5.331 5.331z"></path><path d="M26.462 7.456c0 1.060-0.859 1.919-1.919 1.919s-1.919-0.859-1.919-1.919c0-1.060 0.859-1.919 1.919-1.919s1.919 0.859 1.919 1.919z"></path></svg>
					</a></li>
					<li><a href="https://www.facebook.com/homeschoolpanda/" target="_blank" rel="noopener">
					<svg id="icon-facebook" fill="#fff"  width="30" height="32" viewBox="0 0 512 512"><path d="M288,192v-38.1c0-17.2,3.8-25.9,30.5-25.9H352V64h-55.9c-68.5,0-91.1,31.4-91.1,85.3V192h-45v64h45v192h83V256h56.4l7.6-64 H288z"></path></svg>
					</a></li>
					<li><a href="https://twitter.com/HomeschoolPanda" target="_blank" rel="noopener">
					<svg id="icon-twitter"  fill="#fff" width="30" height="32" viewBox="0 0 512 512" ><path d="M492,109.5c-17.4,7.7-36,12.9-55.6,15.3c20-12,35.4-31,42.6-53.6c-18.7,11.1-39.4,19.2-61.5,23.5 C399.8,75.8,374.6,64,346.8,64c-53.5,0-96.8,43.4-96.8,96.9c0,7.6,0.8,15,2.5,22.1c-80.5-4-151.9-42.6-199.6-101.3 c-8.3,14.3-13.1,31-13.1,48.7c0,33.6,17.2,63.3,43.2,80.7C67,210.7,52,206.3,39,199c0,0.4,0,0.8,0,1.2c0,47,33.4,86.1,77.7,95 c-8.1,2.2-16.7,3.4-25.5,3.4c-6.2,0-12.3-0.6-18.2-1.8c12.3,38.5,48.1,66.5,90.5,67.3c-33.1,26-74.9,41.5-120.3,41.5 c-7.8,0-15.5-0.5-23.1-1.4C62.8,432,113.7,448,168.3,448C346.6,448,444,300.3,444,172.2c0-4.2-0.1-8.4-0.3-12.5 C462.6,146,479,129,492,109.5z"></path></svg>
					</a></li>
					<li><a href="https://www.youtube.com/channel/UCqPXduvxnaqF_KFdBuImu3Q" target="_blank" rel="noopener">
					<svg id="icon-youtube" fill="#fff"  width="30" height="32"  viewBox="0 0 512 512" ><title>youtube</title><g><path d="M508.6,148.8c0-45-33.1-81.2-74-81.2C379.2,65,322.7,64,265,64c-3,0-6,0-9,0s-6,0-9,0c-57.6,0-114.2,1-169.6,3.6 c-40.8,0-73.9,36.4-73.9,81.4C1,184.6-0.1,220.2,0,255.8C-0.1,291.4,1,327,3.4,362.7c0,45,33.1,81.5,73.9,81.5 c58.2,2.7,117.9,3.9,178.6,3.8c60.8,0.2,120.3-1,178.6-3.8c40.9,0,74-36.5,74-81.5c2.4-35.7,3.5-71.3,3.4-107 C512.1,220.1,511,184.5,508.6,148.8z M207,353.9V157.4l145,98.2L207,353.9z"></path></g></svg>
					</a></li>
					<li><a href="http://www.pinterest.com/homeschoolpanda" target="_blank" rel="noopener">
					<svg id="icon-pinterest" fill="#fff" width="30" height="32" viewBox="0 0 32 32" ><path d="M15.985 0c-8.828 0-15.985 7.157-15.985 15.985s7.157 15.985 15.985 15.985c8.828 0 15.985-7.157 15.985-15.985 0-8.83-7.157-15.985-15.985-15.985zM17.232 20.307c-1.169-0.090-1.658-0.669-2.574-1.227-0.504 2.641-1.119 5.175-2.941 6.496-0.563-3.99 0.825-6.987 1.471-10.17-1.099-1.852 0.132-5.575 2.45-4.658 2.851 1.129-2.472 6.88 1.103 7.599 3.73 0.749 5.255-6.476 2.941-8.826-3.345-3.393-9.731-0.078-8.946 4.78 0.192 1.187 1.417 1.547 0.49 3.187-2.14-0.472-2.779-2.162-2.697-4.412 0.132-3.683 3.309-6.26 6.496-6.618 4.030-0.452 7.811 1.479 8.332 5.269 0.589 4.278-1.818 8.914-6.124 8.58z"></path></svg>
					</a></li>
					<li><a href="https://medium.com/the-homeschool-panda-blog" target="_blank" rel="noopener">
					<svg id="icon-medium"  fill="#fff" width="30" height="32" viewBox="0 0 32 32" ><title>medium</title><path d="M0 0h32v32h-32v-32zM7.636 10.702v9.502c0.058 0.343-0.050 0.693-0.291 0.943l-2.258 2.739v0.361h6.403v-0.361l-2.258-2.739c-0.243-0.25-0.358-0.598-0.311-0.943v-8.217l5.62 12.261h0.652l4.827-12.261v9.772c0 0.261 0 0.311-0.171 0.482l-1.736 1.686v0.361h8.43v-0.361l-1.676-1.645c-0.148-0.113-0.221-0.298-0.191-0.482v-12.090c-0.031-0.183 0.043-0.369 0.191-0.482l1.716-1.645v-0.361h-5.941l-4.235 10.565-4.817-10.565h-6.232v0.361l2.007 2.418c0.197 0.178 0.298 0.438 0.271 0.702z"></path></svg>
					</a></li>
				</ul>

				<div style="margin-top:3px">
					<span class="footer-apps-title">Available on</span>
					<div class="footer-apps">
						<a class="footer-app" aria-title="Download Android App" href="https://play.google.com/store/apps/details?id=com.hsp.app&amp;hl=en"><img class="footer-app-icon" src="/../../images/get-it-google-play.svg"></a>
						<a class="footer-app" aria-title="Download iOS App" href="https://itunes.apple.com/us/app/homeschoolpanda/id1237712166?ls=1&amp;mt=8"><img class="footer-app-icon" src="/../../images/ios-app.svg"></a>
					</div>
				</div>
				<ul class="footer-nav">
					
				</ul>
			</div>
			
		</div>
	</footer>

	<script defer src="{relative_path}/assets/nodebb.min.js?{config.cache-buster}"></script>

	<!-- BEGIN scripts -->
	<script defer type="text/javascript" src="{scripts.src}"></script>
	<!-- END scripts -->

	<script>
		window.addEventListener('load', function () {
			require(['forum/footer']);

			<!-- IF useCustomJS -->
			{{customJS}}
			<!-- ENDIF useCustomJS -->
		});
	</script>

	<div class="hide">
	<!-- IMPORT 500-embed.tpl -->
	</div>
</body>
</html>

<div class="subcategory">
	<!-- IF children.length --><h1 class="section-title">[[category:subcategories]]</h1><!-- ENDIF children.length -->

	<ul class="categories" itemscope itemtype="http://www.schema.org/ItemList">
		<!-- BEGIN children -->
		<!-- IMPORT partials/categories/item.tpl -->
		<!-- END children -->
	</ul>
</div>